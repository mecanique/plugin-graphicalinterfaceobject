/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecGraphicalInterfaceObjectPlugin.h"

MecGraphicalInterfaceObjectPlugin::MecGraphicalInterfaceObjectPlugin() : MecPlugin(QString("graphicalinterfaceobject"), __GRAPHICALINTERFACEOBJECT_VERSION__, MecAbstractElement::Object, QString("GraphicalInterface"), QString("Graphical Interface object"))
{
}

MecGraphicalInterfaceObjectPlugin::~MecGraphicalInterfaceObjectPlugin()
{
}

QString MecGraphicalInterfaceObjectPlugin::description() const
{
return QString(tr("Plugin to design a graphical interface for the project."));
}

QString MecGraphicalInterfaceObjectPlugin::copyright() const
{
return QString("Copyright © 2014 – 2015 Quentin VIGNAUD");
}

QString MecGraphicalInterfaceObjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecGraphicalInterfaceObjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

QString MecGraphicalInterfaceObjectPlugin::translators() const
{
return QString("Quentin VIGNAUD <quentin.vignaud@mecanique.cc>\n");
}

MecAbstractElementEditor* MecGraphicalInterfaceObjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecGraphicalInterfaceObjectEditor *tempEditor = new MecGraphicalInterfaceObjectEditor(static_cast<MecAbstractObject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecGraphicalInterfaceObjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecGraphicalInterfaceObjectCompiler *tempCompiler = new MecGraphicalInterfaceObjectCompiler(static_cast<MecAbstractObject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
return 0;
}


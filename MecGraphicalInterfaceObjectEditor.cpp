/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecGraphicalInterfaceObjectEditor.h"

MecGraphicalInterfaceObjectEditor::MecGraphicalInterfaceObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecObjectEditor(Object, MainEditor, Parent, F)
{
tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetVariables));
}

MecGraphicalInterfaceObjectEditor::~MecGraphicalInterfaceObjectEditor()
{
}

bool MecGraphicalInterfaceObjectEditor::canAddVariable() const
{
	return false;
}

QStringList MecGraphicalInterfaceObjectEditor::actualElementsNames() const
{
	QRegExp tempFinderRegex("^set(" + typesAvailable().join('|') + "){1}.+");
	QRegExp tempRemoverRegex("^set(" + typesAvailable().join('|') + "){1}");
	
	QStringList tempList;
	
	for (int i=0 ; i < element()->childElements().size() ; i++)
		{
		if (tempFinderRegex.exactMatch(element()->childElements().at(i)->elementName()))
			{
			tempList.append(element()->childElements().at(i)->elementName().remove(tempRemoverRegex));
			}
		}
	
	return tempList;
}

QStringList MecGraphicalInterfaceObjectEditor::typesAvailable()
{
	return QStringList() 
		<< "PushButton"
		<< "CheckBox"
		<< "RadioButton"
		<< "ComboBox"
		<< "SpinBox"
		<< "DoubleSpinBox"
		<< "Slider"
		<< "Dial"
		<< "LineEdit"
		<< "Label"
		<< "ProgressBar"
		<< "LCDNumber"
		;
}
	
MecAbstractElementEditor* MecGraphicalInterfaceObjectEditor::newSubEditor(MecAbstractElement *Element)
{
if (Element->elementRole() == MecAbstractElement::Function)
	{
	MecElementEditor *tempEditor = new MecGraphicalInterfaceFunctionEditor(static_cast<MecAbstractFunction*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetFunctions->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	return tempEditor;
	}
else if (Element->elementRole() == MecAbstractElement::Signal)
	{
	MecElementEditor *tempEditor = new MecGraphicalInterfaceSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	return tempEditor;
	}
else return 0;
}
	
void MecGraphicalInterfaceObjectEditor::addFunction()
{
	chooseGraphicalElement();
}

void MecGraphicalInterfaceObjectEditor::addSignal()
{
	chooseGraphicalElement();
}

void MecGraphicalInterfaceObjectEditor::addVariable() {}

void MecGraphicalInterfaceObjectEditor::chooseGraphicalElement()
{
	MecGraphicalInterfaceObjectEditorChooser tempChooser(this);
	
	if (tempChooser.exec() == QDialog::Accepted)
		{
		MecAbstractElement *tempElement = mainEditor()->read(":/share/base/object/GraphicalInterface/" + tempChooser.choice() + ".mec");
		
		QString tempName = tempChooser.nameChoosed();
		while (actualElementsNames().contains(tempName)) tempName.prepend('_');
		
		while (tempElement->childElements().size() > 0)
			{
			if (tempElement->childElements().first()->elementRole() == MecAbstractElement::Function)
				tempElement->childElements().first()->setElementName(tempElement->childElements().first()->elementName() + tempName);
			else if (tempElement->childElements().first()->elementRole() == MecAbstractElement::Signal)
				tempElement->childElements().first()->setElementName(tempName + tempElement->childElements().first()->elementName());
			
			connect(tempElement->childElements().first(), SIGNAL(destroyed(MecAbstractElement*)), SLOT(removeGraphicalElement(MecAbstractElement*)));
			tempElement->childElements().first()->setParentElement(element());
			}
		
		delete tempElement;
		
		mainEditor()->addEditStep(tr("Add the graphical element “%1”").arg(tempChooser.choice()));
		}
}

void MecGraphicalInterfaceObjectEditor::removeGraphicalElement(MecAbstractElement *Element)
{
	MecAbstractElement *tempElement = Element;
	
	
	QRegExp tempRemoverRegex("((^set(" + typesAvailable().join('|') + "))|(^set)|(^get)|(Changed$))");
	
	QString graphicalElementName = tempElement->elementName().remove(tempRemoverRegex);
	
	QRegExp tempFinderRegex("((^set(" + typesAvailable().join('|') + ")" + graphicalElementName + ")|(^set" + graphicalElementName + ")|(^get" + graphicalElementName + ")|(" + graphicalElementName + "Changed$))");
	
	QList<MecAbstractElement*> tempElementList(element()->childElements());
	for (int i=0 ; i < tempElementList.size() ; i++)
		{
		if (tempFinderRegex.exactMatch(tempElementList.at(i)->elementName())
			&& tempElementList.at(i)->elementName() != Element->elementName()) {
			//Pour éviter des appels en boucle sur cette méthode.
			disconnect(tempElementList.at(i), SIGNAL(destroyed(MecAbstractElement*)), this, SLOT(removeGraphicalElement(MecAbstractElement*)));
			delete tempElementList.at(i);
			}
		}
}

MecGraphicalInterfaceObjectEditorChooser::MecGraphicalInterfaceObjectEditorChooser(QWidget * Parent, Qt::WindowFlags F) : QDialog(Parent, F)
{
	pushButton = new QPushButton(tr("Click me!"), this);
	buttonPushButton = new QPushButton("PushButton", this);
		connect(buttonPushButton, SIGNAL(clicked(bool)), SLOT(choose()));
	
	checkBox = new QCheckBox(tr("Check me!"), this);
	buttonCheckBox = new QPushButton("CheckBox", this);
		connect(buttonCheckBox, SIGNAL(clicked(bool)), SLOT(choose()));
	
	radioButton = new QRadioButton(tr("Select me!"), this);
	buttonRadioButton = new QPushButton("RadioButton", this);
		connect(buttonRadioButton, SIGNAL(clicked(bool)), SLOT(choose()));
	
	comboBox = new QComboBox(this);
		comboBox->addItems(QStringList() << tr("First choice") << tr("Second choice") << tr("Third choice"));
	buttonComboBox = new QPushButton("ComboBox", this);
		connect(buttonComboBox, SIGNAL(clicked(bool)), SLOT(choose()));
	
	spinBox = new QSpinBox(this);
		spinBox->setSuffix(tr(" unit"));
		spinBox->setRange(0, 100);
	buttonSpinBox = new QPushButton("SpinBox", this);
		connect(buttonSpinBox, SIGNAL(clicked(bool)), SLOT(choose()));
	
	doubleSpinBox = new QDoubleSpinBox(this);
		doubleSpinBox->setSuffix(tr(" unit"));
		doubleSpinBox->setRange(0, 100);
		doubleSpinBox->setDecimals(2);
		doubleSpinBox->setSingleStep(0.01);
	buttonDoubleSpinBox = new QPushButton("DoubleSpinBox", this);
		connect(buttonDoubleSpinBox, SIGNAL(clicked(bool)), SLOT(choose()));
	
	slider = new QSlider(Qt::Horizontal, this);
		slider->setRange(0, 100);
	buttonSlider = new QPushButton("Slider", this);
		connect(buttonSlider, SIGNAL(clicked(bool)), SLOT(choose()));
	
	dial = new QDial(this);
		dial->setRange(0, 100);
	buttonDial = new QPushButton("Dial", this);
		connect(buttonDial, SIGNAL(clicked(bool)), SLOT(choose()));
	
	lineEdit = new QLineEdit(tr("Edit me!"), this);
	buttonLineEdit = new QPushButton("LineEdit", this);
		connect(buttonLineEdit, SIGNAL(clicked(bool)), SLOT(choose()));
	
	label = new QLabel(tr("Lorem ipsum"), this);
	buttonLabel = new QPushButton("Label", this);
		connect(buttonLabel, SIGNAL(clicked(bool)), SLOT(choose()));
	
	progressBar = new QProgressBar(this);
		progressBar->setRange(0, 100);
		progressBar->setValue(30);
		progressBar->setTextVisible(true);
	buttonProgressBar = new QPushButton("ProgressBar", this);
		connect(buttonProgressBar, SIGNAL(clicked(bool)), SLOT(choose()));
	
	LCDNumber = new QLCDNumber(this);
		LCDNumber->setDecMode();
		LCDNumber->setDigitCount(3);
		LCDNumber->display(42);
	buttonLCDNumber = new QPushButton("LCDNumber", this);
		connect(buttonLCDNumber, SIGNAL(clicked(bool)), SLOT(choose()));
	
	
	labelName = new QLabel(tr("New element name"), this);
	regExpValidatorName = new QRegExpValidator(MecAbstractElement::standardSyntax(), this);
	lineEditName = new QLineEdit(this);
		lineEditName->setText("Element");
		lineEditName->setValidator(regExpValidatorName);
		connect(lineEditName, SIGNAL(textEdited(QString)), SLOT(checkEditName(QString)));
		
	frameTopLine = new QFrame(this);
		frameTopLine->setFrameStyle(QFrame::HLine | QFrame::Raised);
	frameBottomLine = new QFrame(this);
		frameBottomLine->setFrameStyle(QFrame::HLine | QFrame::Raised);
	
	
	buttonCancel = new QPushButton(tr("Cancel"), this);
		connect(buttonCancel, SIGNAL(clicked(bool)), SLOT(reject()));
	
	
	layoutMain = new QGridLayout(this);
		layoutMain->addWidget(labelName, 0, 0);
		layoutMain->addWidget(lineEditName, 0, 1);
		layoutMain->addWidget(frameTopLine, 1, 0, 1, 2);
		
		layoutMain->addWidget(pushButton, 2, 0);
		layoutMain->addWidget(buttonPushButton, 2, 1);
		layoutMain->addWidget(checkBox, 3, 0);
		layoutMain->addWidget(buttonCheckBox, 3, 1);
		layoutMain->addWidget(radioButton, 4, 0);
		layoutMain->addWidget(buttonRadioButton, 4, 1);
		layoutMain->addWidget(comboBox, 5, 0);
		layoutMain->addWidget(buttonComboBox, 5, 1);
		layoutMain->addWidget(spinBox, 6, 0);
		layoutMain->addWidget(buttonSpinBox, 6, 1);
		layoutMain->addWidget(doubleSpinBox, 7, 0);
		layoutMain->addWidget(buttonDoubleSpinBox, 7, 1);
		layoutMain->addWidget(slider, 8, 0);
		layoutMain->addWidget(buttonSlider, 8, 1);
		layoutMain->addWidget(dial, 9, 0);
		layoutMain->addWidget(buttonDial, 9, 1);
		layoutMain->addWidget(lineEdit, 10, 0);
		layoutMain->addWidget(buttonLineEdit, 10, 1);
		layoutMain->addWidget(label, 11, 0);
		layoutMain->addWidget(buttonLabel, 11, 1);
		layoutMain->addWidget(progressBar, 12, 0);
		layoutMain->addWidget(buttonProgressBar, 12, 1);
		layoutMain->addWidget(LCDNumber, 13, 0);
		layoutMain->addWidget(buttonLCDNumber, 13, 1);
		
		layoutMain->addWidget(frameBottomLine, 14, 0, 1, 2);
		layoutMain->addWidget(buttonCancel, 15, 1);
}

MecGraphicalInterfaceObjectEditorChooser::~MecGraphicalInterfaceObjectEditorChooser()
{
}

QString MecGraphicalInterfaceObjectEditorChooser::choice() const
{
	return m_choice;
}

QString MecGraphicalInterfaceObjectEditorChooser::nameChoosed() const
{
	return m_nameChoosed;
}

void MecGraphicalInterfaceObjectEditorChooser::choose()
{
	QAbstractButton *callerButton = qobject_cast<QAbstractButton*>(sender());
	
	if (callerButton != 0)
		{
		m_choice = callerButton->text();
		m_nameChoosed = lineEditName->text();
		accept();
		}
}

void MecGraphicalInterfaceObjectEditorChooser::checkEditName(QString Text)
{
	if (Text.isEmpty()) (lineEditName->isUndoAvailable()) ? lineEditName->undo() : lineEditName->setText("Element");
}





/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECGRAPHICALINTERFACEOBJECTPLUGIN_H__
#define __MECGRAPHICALINTERFACEOBJECTPLUGIN_H__

#include <MecPlugin.h>
///Version du plugin.
#define __GRAPHICALINTERFACEOBJECT_VERSION__ 1.001

#include "MecGraphicalInterfaceObjectCompiler.h"
#include "MecGraphicalInterfaceObjectEditor.h"

/**
\brief	Classe d'interface du plugin « Graphical Interface object ».

\par	Propriétés
Nom : graphicalinterfaceobject \n
Version : 1.001 \n
Rôle : Object \n
Type : GraphicalInterface \n
Titre : Graphical Interface object \n
*/
class MecGraphicalInterfaceObjectPlugin : public MecPlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Mecanique.MecAbstractPlugin" FILE "metadata.json")

public:
	///Constructeur.
	MecGraphicalInterfaceObjectPlugin();
	///Destructeur.
	~MecGraphicalInterfaceObjectPlugin();

	///Retourne la description du plugin.
	QString description() const;
	///Retourne le copyright du plugin.
	QString copyright() const;
	///Retourne la liste des développeurs du plugin.
	QString developpers() const;
	///Retourne la liste des documentalistes.
	QString documentalists() const;
	///Retourne la liste des traducteurs.
	QString translators() const;

	/**
	\brief	Retourne un éditeur de l'élément spécifié.
	\param	Element	Doit correspondre aux données du plugin, sinon 0 est retourné.
	*/
	MecAbstractElementEditor* elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor);
	/**
	\brief	Retourne un compilateur de l'élément spécifié.
	\param	Element	Doit correspondre aux données du plugin, sinon 0 est retourné.
	*/
	MecAbstractElementCompiler* elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler);
};

#endif /* __MECGRAPHICALINTERFACEOBJECTPLUGIN_H__ */


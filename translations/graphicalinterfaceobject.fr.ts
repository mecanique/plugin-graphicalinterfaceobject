<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MecElementEditor</name>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="32"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="40"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="172"/>
        <source>Add the object “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="182"/>
        <source>Add the function “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="192"/>
        <source>Add the signal “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="202"/>
        <source>Add the variable “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="297"/>
        <source>Change name of “%1” to “%2”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecElementEditor.cpp" line="310"/>
        <source>Change type of “%1” to “%2” from “%3”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecFunctionEditor</name>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="33"/>
        <source>Return type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="45"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="65"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="75"/>
        <location filename="../../plugin/MecFunctionEditor.cpp" line="162"/>
        <source>Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecGraphicalInterfaceObjectEditor</name>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="127"/>
        <source>Add the graphical element “%1”</source>
        <translation>Ajout de l&apos;élément graphique « %1 »</translation>
    </message>
</context>
<context>
    <name>MecGraphicalInterfaceObjectEditorChooser</name>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="156"/>
        <source>Click me!</source>
        <translation>Cliquez !</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="160"/>
        <source>Check me!</source>
        <translation>Cochez !</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="164"/>
        <source>Select me!</source>
        <translation>Séléectionnez !</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="169"/>
        <source>First choice</source>
        <translation>Premier choix</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="169"/>
        <source>Second choice</source>
        <translation>Deuxième choix</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="169"/>
        <source>Third choice</source>
        <translation>Troisième choix</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="174"/>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="180"/>
        <source> unit</source>
        <translation> unité</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="197"/>
        <source>Edit me!</source>
        <translation>Éditez !</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="201"/>
        <source>Lorem ipsum</source>
        <translation>Lorem ipsum</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="220"/>
        <source>New element name</source>
        <translation>Nom du nouvel élément</translation>
    </message>
    <message>
        <location filename="../MecGraphicalInterfaceObjectEditor.cpp" line="233"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>MecGraphicalInterfaceObjectPlugin</name>
    <message>
        <location filename="../MecGraphicalInterfaceObjectPlugin.cpp" line="32"/>
        <source>Plugin to design a graphical interface for the project.</source>
        <translation>Greffon de conception d&apos;interfaces graphiques pour les projets.</translation>
    </message>
</context>
<context>
    <name>MecObjectEditor</name>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="42"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="57"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="67"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="222"/>
        <source>Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="77"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="223"/>
        <source>Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecObjectEditor.cpp" line="96"/>
        <location filename="../../plugin/MecObjectEditor.cpp" line="224"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecProjectEditor</name>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="30"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="35"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="45"/>
        <source>Synopsis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="56"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="66"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="76"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="265"/>
        <source>Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="86"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="266"/>
        <source>Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="105"/>
        <location filename="../../plugin/MecProjectEditor.cpp" line="267"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="407"/>
        <source>Change title of the project</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecProjectEditor.cpp" line="415"/>
        <source>Change synopsis of the project</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalConnectionsList</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="93"/>
        <source>Remove connections with “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="108"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalEditor</name>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="190"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="204"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="224"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="231"/>
        <source>Add connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="233"/>
        <source>Remove connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="241"/>
        <source>Connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../plugin/MecSignalEditor.cpp" line="403"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>

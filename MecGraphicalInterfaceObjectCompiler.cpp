/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecGraphicalInterfaceObjectCompiler.h"

MecGraphicalInterfaceObjectCompiler::MecGraphicalInterfaceObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler) : MecObjectCompiler(Object, MainCompiler)
{
m_containedElements = containedElements();
}

MecGraphicalInterfaceObjectCompiler::~MecGraphicalInterfaceObjectCompiler()
{
}

QMap<QString, QString> MecGraphicalInterfaceObjectCompiler::containedElements() const
{
	QRegExp tempFinderRegex("^set(" + typesAvailable().join('|') + "){1}.+");
	QRegExp tempRemoverRegex("^set(" + typesAvailable().join('|') + "){1}");
	
	QMap<QString, QString> tempMap;
	
	for (int i=0 ; i < element()->childElements().size() ; i++)
		{
		if (tempFinderRegex.exactMatch(element()->childElements().at(i)->elementName()))
			{
			QString tempName(element()->childElements().at(i)->elementName().remove(tempRemoverRegex));
			QString tempType(element()->childElements().at(i)->elementName().remove(QRegExp("^set")).remove(QRegExp(tempName + "$")));
			tempMap.insert(tempName, tempType);
			}
		}
	
	return tempMap;
}

QStringList MecGraphicalInterfaceObjectCompiler::typesAvailable()
{
	return QStringList() 
		<< "PushButton"
		<< "CheckBox"
		<< "RadioButton"
		<< "ComboBox"
		<< "SpinBox"
		<< "DoubleSpinBox"
		<< "Slider"
		<< "Dial"
		<< "LineEdit"
		<< "Label"
		<< "ProgressBar"
		<< "LCDNumber"
		;
}
	
QList<QResource*> MecGraphicalInterfaceObjectCompiler::resources()
{
QList<QResource*> tempList;
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalInterface.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalInterface.cpp"));
tempList.append(new QResource(":/share/icons/types/GraphicalInterface.png"));
//Éléments graphiques.
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/CheckBox.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/CheckBox.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/ComboBox.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/ComboBox.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/Dial.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/Dial.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/DoubleSpinBox.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/DoubleSpinBox.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/GraphicalElement.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/GraphicalElement.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/Label.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/Label.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/LCDNumber.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/LCDNumber.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/LineEdit.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/LineEdit.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/ProgressBar.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/ProgressBar.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/PushButton.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/PushButton.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/RadioButton.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/RadioButton.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/Slider.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/Slider.h"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/SpinBox.cpp"));
tempList.append(new QResource(":/src/GraphicalInterface/GraphicalElements/SpinBox.h"));
//Traductions.
tempList.append(new QResource(":/share/translations/GraphicalInterface.fr.qm"));
//Fait partie d'un plugin standard.
tempList.append(new QResource(":/src/mecanique/string.h"));
return tempList;
}

QString MecGraphicalInterfaceObjectCompiler::projectInstructions()
{
return QString("QT += network\n\
HEADERS += GraphicalInterface/GraphicalInterface.h \\\n\
GraphicalInterface/GraphicalElements/CheckBox.h \\\n\
GraphicalInterface/GraphicalElements/ComboBox.h \\\n\
GraphicalInterface/GraphicalElements/Dial.h \\\n\
GraphicalInterface/GraphicalElements/DoubleSpinBox.h \\\n\
GraphicalInterface/GraphicalElements/GraphicalElement.h \\\n\
GraphicalInterface/GraphicalElements/Label.h \\\n\
GraphicalInterface/GraphicalElements/LCDNumber.h \\\n\
GraphicalInterface/GraphicalElements/LineEdit.h \\\n\
GraphicalInterface/GraphicalElements/ProgressBar.h \\\n\
GraphicalInterface/GraphicalElements/PushButton.h \\\n\
GraphicalInterface/GraphicalElements/RadioButton.h \\\n\
GraphicalInterface/GraphicalElements/Slider.h \\\n\
GraphicalInterface/GraphicalElements/SpinBox.h \n\n\
SOURCES += GraphicalInterface/GraphicalInterface.cpp \\\n\
GraphicalInterface/GraphicalElements/CheckBox.cpp \\\n\
GraphicalInterface/GraphicalElements/ComboBox.cpp \\\n\
GraphicalInterface/GraphicalElements/Dial.cpp \\\n\
GraphicalInterface/GraphicalElements/DoubleSpinBox.cpp \\\n\
GraphicalInterface/GraphicalElements/GraphicalElement.cpp \\\n\
GraphicalInterface/GraphicalElements/Label.cpp \\\n\
GraphicalInterface/GraphicalElements/LCDNumber.cpp \\\n\
GraphicalInterface/GraphicalElements/LineEdit.cpp \\\n\
GraphicalInterface/GraphicalElements/ProgressBar.cpp \\\n\
GraphicalInterface/GraphicalElements/PushButton.cpp \\\n\
GraphicalInterface/GraphicalElements/RadioButton.cpp \\\n\
GraphicalInterface/GraphicalElements/Slider.cpp \\\n\
GraphicalInterface/GraphicalElements/SpinBox.cpp \n\n\
");
}
	
QString MecGraphicalInterfaceObjectCompiler::header()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2014\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "#ifndef __" + object()->elementName().toUpper() + "_H__\n#define __" + object()->elementName().toUpper() + "_H__\n\n#include \"GraphicalInterface/GraphicalInterface.h\"\n";

tempString += "#include \"GraphicalInterface/GraphicalElements/CheckBox.h\"\n\
#include \"GraphicalInterface/GraphicalElements/ComboBox.h\"\n\
#include \"GraphicalInterface/GraphicalElements/Dial.h\"\n\
#include \"GraphicalInterface/GraphicalElements/DoubleSpinBox.h\"\n\
#include \"GraphicalInterface/GraphicalElements/Label.h\"\n\
#include \"GraphicalInterface/GraphicalElements/LCDNumber.h\"\n\
#include \"GraphicalInterface/GraphicalElements/LineEdit.h\"\n\
#include \"GraphicalInterface/GraphicalElements/ProgressBar.h\"\n\
#include \"GraphicalInterface/GraphicalElements/PushButton.h\"\n\
#include \"GraphicalInterface/GraphicalElements/RadioButton.h\"\n\
#include \"GraphicalInterface/GraphicalElements/Slider.h\"\n\
#include \"GraphicalInterface/GraphicalElements/SpinBox.h\"\n\n";

for (int i=0 ; i < recConcreteSubCompilers().size() ; i++)
	{
	tempString += recConcreteSubCompilers().at(i)->preprocessorInstructions();
	}

tempString += "\nclass " + object()->elementName() + " : public GraphicalInterface\n{\nQ_OBJECT\n\tpublic:\n";
tempString += object()->elementName() + "(Project* const Project);\n~" + object()->elementName() + "();\n\n";

tempString += "\tpublic slots:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Function) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tsignals:\n";

for (int i=0 ; i < concreteSubCompilers().size() ; i++)
	{
	if (concreteSubCompilers().at(i)->element()->elementRole() == MecAbstractElement::Signal) tempString += concreteSubCompilers().at(i)->headerInstructions();
	}

tempString += "\tprivate:\n";

QMapIterator<QString, QString> tempIterator(m_containedElements);
while (tempIterator.hasNext()) {
	tempIterator.next();
	tempString += tempIterator.value() + " *ge_" + tempIterator.key() + ";\n";
}
tempString += "};\n\n\n#endif /* __" + object()->elementName().toUpper() + "_H__ */\n\n";

return tempString;
}

QString MecGraphicalInterfaceObjectCompiler::source()
{
QString tempString;

tempString = "/*\n\
© Quentin VIGNAUD, 2014\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\n";

tempString += "\n#include \"" + object()->elementName() + ".h\"\n\n";

tempString += object()->elementName() + "::" + object()->elementName() + "(Project* const Project) : GraphicalInterface(\"" + object()->elementName() + "\", Project)\n{\n";

//Déclaration
QMapIterator<QString, QString> tempIterator(m_containedElements);
while (tempIterator.hasNext()) {
	tempIterator.next();
	tempString += "ge_" + tempIterator.key() + " = new " + tempIterator.value() + "(\"" + tempIterator.key() + "\");\n";
	tempString += "set" + tempIterator.value() + tempIterator.key() + "();\n";
	tempString += "addGraphicalElement(ge_" + tempIterator.key() + ");\n";
	
	//Signaux
	if (tempIterator.value() == "CheckBox") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(bool, bool)), SIGNAL(" + tempIterator.key() + "Changed(bool, bool)));\n";
	} else if (tempIterator.value() == "ComboBox") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(string, bool)), SIGNAL(" + tempIterator.key() + "Changed(string, bool)));\n";
	} else if (tempIterator.value() == "Dial") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(int, bool)), SIGNAL(" + tempIterator.key() + "Changed(int, bool)));\n";
	} else if (tempIterator.value() == "DoubleSpinBox") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(double, bool)), SIGNAL(" + tempIterator.key() + "Changed(double, bool)));\n";
	} else if (tempIterator.value() == "LCDNumber") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(double)), SIGNAL(" + tempIterator.key() + "Changed(double)));\n";
	} else if (tempIterator.value() == "LineEdit") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(string, bool)), SIGNAL(" + tempIterator.key() + "Changed(string, bool)));\n";
	} else if (tempIterator.value() == "ProgressBar") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(int)), SIGNAL(" + tempIterator.key() + "Changed(int)));\n";
	} else if (tempIterator.value() == "PushButton") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed()), SIGNAL(" + tempIterator.key() + "Changed()));\n";
	} else if (tempIterator.value() == "RadioButton") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(string, bool)), SIGNAL(" + tempIterator.key() + "Changed(string, bool)));\n";
	} else if (tempIterator.value() == "Slider") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(int, bool)), SIGNAL(" + tempIterator.key() + "Changed(int, bool)));\n";
	} else if (tempIterator.value() == "SpinBox") {
		tempString += "connect(ge_" + tempIterator.key() + ", SIGNAL(changed(int, bool)), SIGNAL(" + tempIterator.key() + "Changed(int, bool)));\n";
	}
}

tempString += "}\n\n";

tempString += object()->elementName() + "::~" + object()->elementName() + "()\n{\n}\n\n";

//Fonctions
tempIterator.toFront();
while (tempIterator.hasNext()) {
	tempIterator.next();

	if (tempIterator.value() == "CheckBox") {
		tempString += "void " + object()->elementName() + "::setCheckBox" + tempIterator.key() + "(bool enabled, string text) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(bool checked) {\n\
		ge_" + tempIterator.key() + "->setValue(checked);\n}\n";
		tempString += "bool " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "ComboBox") {
		tempString += "void " + object()->elementName() + "::setComboBox" + tempIterator.key() + "(bool enabled, string text, string values) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, values);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(string value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "string " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "Dial") {
		tempString += "void " + object()->elementName() + "::setDial" + tempIterator.key() + "(bool enabled, string text, int minimum, int maximum, int step, bool wrapping) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, minimum, maximum, step, wrapping);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(int value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "int " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "DoubleSpinBox") {
		tempString += "void " + object()->elementName() + "::setDoubleSpinBox" + tempIterator.key() + "(bool enabled, string text, double minimum, double maximum, double step, uint decimals, string prefix, string suffix) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, minimum, maximum, step, decimals, prefix, suffix);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(double value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "double " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "Label") {
		tempString += "void " + object()->elementName() + "::setLabel" + tempIterator.key() + "(string text) {\n\
		ge_" + tempIterator.key() + "->setProperties(text);\n}\n";
		
	} else if (tempIterator.value() == "LCDNumber") {
		tempString += "void " + object()->elementName() + "::setLCDNumber" + tempIterator.key() + "(bool enabled, string text, uint digits, uint base) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, digits, base);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(double value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "double " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "LineEdit") {
		tempString += "void " + object()->elementName() + "::setLineEdit" + tempIterator.key() + "(bool enabled, string text) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(string value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "string " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "ProgressBar") {
		tempString += "void " + object()->elementName() + "::setProgressBar" + tempIterator.key() + "(bool enabled, string text, int minimum, int maximum, string format, bool inverted) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, minimum, maximum, format, inverted);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(int value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "int " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "PushButton") {
		tempString += "void " + object()->elementName() + "::setPushButton" + tempIterator.key() + "(bool enabled, string text) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text);\n}\n";
		
	} else if (tempIterator.value() == "RadioButton") {
		tempString += "void " + object()->elementName() + "::setRadioButton" + tempIterator.key() + "(bool enabled, string text, string values) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, values);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(string value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "string " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "Slider") {
		tempString += "void " + object()->elementName() + "::setSlider" + tempIterator.key() + "(bool enabled, string text, int minimum, int maximum, int step) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, minimum, maximum, step);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(int value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "int " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	} else if (tempIterator.value() == "SpinBox") {
		tempString += "void " + object()->elementName() + "::setSpinBox" + tempIterator.key() + "(bool enabled, string text, int minimum, int maximum, int step, string prefix, string suffix) {\n\
		ge_" + tempIterator.key() + "->setProperties(enabled, text, minimum, maximum, step, prefix, suffix);\n}\n";
		tempString += "void " + object()->elementName() +  "::set" + tempIterator.key() + "(int value) {\n\
		ge_" + tempIterator.key() + "->setValue(value);\n}\n";
		tempString += "int " + object()->elementName() +  "::get" + tempIterator.key() + "() {\n\
		return ge_" + tempIterator.key() + "->getValue();\n}\n";
		
	}
}

return tempString;
}



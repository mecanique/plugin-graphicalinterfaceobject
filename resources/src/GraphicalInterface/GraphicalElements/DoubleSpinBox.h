/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __DOUBLESPINBOX_H__
#define __DOUBLESPINBOX_H__

#include "GraphicalElement.h"
#include <QDoubleSpinBox>

/*
Classe de représentation d'une doublespinbox.
*/
class DoubleSpinBox : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	DoubleSpinBox(QString Text, QWidget *Parent=0);
	///Destructeur.
	~DoubleSpinBox();
	
	/**
	Fixe les propriétés de la doublespinbox.
	*/
	void setProperties(bool Enabled, QString Text, double Min, double Max, double Step, unsigned int Decimals, QString Prefix, QString Suffix);
	/**
	Fixe la valeur de la doublespinbox.
	*/
	void setValue(double Value);
	/**
	Retourne la valeur de la doublespinbox.
	*/
	double getValue();
	
	signals:
	/**
	Est émis lorsque la doublespinbox change de valeur.
	*/
	void changed(double Value, bool User);
	
	private:
	///DoubleSpinBox.
	QDoubleSpinBox *doubleSpinBox;
	
	private slots:
	///Connecté à QDoubleSpinBox::valueChanged(int)
	void doubleSpinBoxStateChanged(double Value);
};

#endif /* __DOUBLESPINBOX_H__ */


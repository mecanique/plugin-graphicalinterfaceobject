/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Slider.h"

Slider::Slider(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	slider = new QSlider(Qt::Horizontal, this);
	setAttached(slider);
	connect(slider, SIGNAL(valueChanged(int)), SLOT(sliderStateChanged(int)));
}

Slider::~Slider()
{
}
	
void Slider::setProperties(bool Enabled, QString Text, int Min, int Max, int Step)
{
	slider->setEnabled(Enabled);
	setText(Text);
	slider->setRange(Min, Max);
	slider->setSingleStep(Step);
}

void Slider::setValue(int Value)
{
	blockSignals(true);
	slider->setValue(Value);
	blockSignals(false);
	emit changed(Value, false);
}

int Slider::getValue()
{
	return slider->value();
}

void Slider::sliderStateChanged(int Value)
{
	emit changed(Value, true);
}



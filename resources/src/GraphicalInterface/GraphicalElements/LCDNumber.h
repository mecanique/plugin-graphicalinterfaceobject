/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __LCDNUMBER_H__
#define __LCDNUMBER_H__

#include "GraphicalElement.h"
#include <QLCDNumber>

/*
Classe de représentation d'un nombre LCD.
*/
class LCDNumber : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	LCDNumber(QString Text, QWidget *Parent=0);
	///Destructeur.
	~LCDNumber();
	
	/**
	Fixe les propriétés du LCD.
	*/
	void setProperties(bool Enabled, QString Text, unsigned int Digits, unsigned int Base);
	/**
	Fixe la valeur du LCD.
	*/
	void setValue(double Value);
	/**
	Retourne la valeur du LCD.
	*/
	double getValue();
	
	signals:
	/**
	Est émis lorsque le LCD change de valeur.
	*/
	void changed(double Value);
	
	private:
	///LCDNumber.
	QLCDNumber *lcdNumber;
};

#endif /* __LCDNUMBER_H__ */


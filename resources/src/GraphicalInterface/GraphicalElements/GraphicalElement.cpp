/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "GraphicalElement.h"

GraphicalElement::GraphicalElement(QString Text, QWidget *Parent) : QWidget(Parent)
{
	layout = new QHBoxLayout(this);
	
	label = new QLabel(Text, this);
	
	layout->addWidget(label);
	layout->setSpacing(0);
}

GraphicalElement::~GraphicalElement()
{
}
	
void GraphicalElement::setAttached(QWidget *Widget)
{
	delete layout->takeAt(1);
	layout->addWidget(Widget);
	label->setMinimumWidth(150);
	label->setMaximumWidth(150);
}

void GraphicalElement::setText(QString Text)
{
	label->setText(Text);
}



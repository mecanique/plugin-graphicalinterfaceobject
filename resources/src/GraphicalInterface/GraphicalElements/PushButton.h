/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __PUSHBUTTON_H__
#define __PUSHBUTTON_H__

#include "GraphicalElement.h"
#include <QPushButton>

/*
Classe de représentation d'un pushbutton.
*/
class PushButton : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	PushButton(QString Text, QWidget *Parent=0);
	///Destructeur.
	~PushButton();
	
	/**
	Fixe les propriétés du pushbutton.
	*/
	void setProperties(bool Enabled, QString Text);
	
	signals:
	/**
	Est émis lorsque le pushbutton est cliqué.
	*/
	void changed();
	
	private:
	///PushButton.
	QPushButton *pushButton;
	
	private slots:
	///Connecté à QPushButton::clicked()
	void pushButtonStateChanged();
};

#endif /* __PUSHBUTTON_H__ */


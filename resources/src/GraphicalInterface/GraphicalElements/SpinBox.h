/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __SPINBOX_H__
#define __SPINBOX_H__

#include "GraphicalElement.h"
#include <QSpinBox>

/*
Classe de représentation d'une spinbox.
*/
class SpinBox : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	SpinBox(QString Text, QWidget *Parent=0);
	///Destructeur.
	~SpinBox();
	
	/**
	Fixe les propriétés de la spinbox.
	*/
	void setProperties(bool Enabled, QString Text, int Min, int Max, int Step, QString Prefix, QString Suffix);
	/**
	Fixe la valeur de la spinbox.
	*/
	void setValue(int Value);
	/**
	Retourne la valeur de la spinbox.
	*/
	int getValue();
	
	signals:
	/**
	Est émis lorsque la spinbox change de valeur.
	*/
	void changed(int Value, bool User);
	
	private:
	///SpinBox.
	QSpinBox *spinBox;
	
	private slots:
	///Connecté à QSpinBox::valueChanged(int)
	void spinBoxStateChanged(int Value);
};

#endif /* __SPINBOX_H__ */


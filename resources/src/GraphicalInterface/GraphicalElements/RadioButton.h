/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __RADIOBUTTON_H__
#define __RADIOBUTTON_H__

#include "GraphicalElement.h"
#include <QButtonGroup>
#include <QRadioButton>
#include "../../mecanique/string.h"

/*
Classe de représentation d'un bloc radiobutton.
*/
class RadioButton : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	RadioButton(QString Text, QWidget *Parent=0);
	///Destructeur.
	~RadioButton();
	
	/**
	Fixe les propriétés d'un bloc radiobutton.
	*/
	void setProperties(bool Enabled, QString Text, QString Values);
	/**
	Fixe la valeur d'un bloc radiobutton.
	*/
	void setValue(QString Value);
	/**
	Retourne la valeur d'un bloc radiobutton.
	*/
	QString getValue();
	
	signals:
	/**
	Est émis lorsque le bloc radiobutton change de valeur.
	*/
	void changed(string Value, bool User);
	
	private:
	///Widget d'affichage du groupe de boutons.
	QWidget *widget;
	///Layout organisationnel.
	QGridLayout *layout;
	///ButtonGroup
	QButtonGroup *group;
	
	private slots:
	///Connecté à QButtonGroup::buttonClicked(QAbstractButton*)
	void buttonGroupStateChanged(QAbstractButton *Button);
};

#endif /* __RADIOBUTTON_H__ */


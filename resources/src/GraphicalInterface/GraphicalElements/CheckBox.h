/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __CHECKBOX_H__
#define __CHECKBOX_H__

#include "GraphicalElement.h"
#include <QCheckBox>

/*
Classe de représentation d'une check box.
*/
class CheckBox : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	CheckBox(QString Text, QWidget *Parent=0);
	///Destructeur.
	~CheckBox();
	
	/**
	Fixe les propriétés de la check box.
	*/
	void setProperties(bool Enabled, QString Text);
	/**
	Fixe la valeur de la check box.
	*/
	void setValue(bool Checked);
	/**
	Retourne la valeur de la check box.
	*/
	bool getValue();
	
	signals:
	/**
	Est émis lorsque la check box change de valeur.
	*/
	void changed(bool Enabled, bool User);
	
	private:
	///Check box.
	QCheckBox *checkBox;
	
	private slots:
	///Connecté à QCheckBox::stateChanged()
	void checkStateChanged(int State);
};

#endif /* __CHECKBOX_H__ */


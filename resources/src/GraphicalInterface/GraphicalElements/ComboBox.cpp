/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "ComboBox.h"

ComboBox::ComboBox(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	comboBox = new QComboBox(this);
	setAttached(comboBox);
	connect(comboBox, SIGNAL(currentTextChanged(QString)), SLOT(comboStateChanged(QString)));
}

ComboBox::~ComboBox()
{
}
	
void ComboBox::setProperties(bool Enabled, QString Text, QString Values)
{
	comboBox->setEnabled(Enabled);
	setText(Text);
	comboBox->clear();
	comboBox->addItems(Values.split(';'));
}

void ComboBox::setValue(QString Value)
{
	blockSignals(true);
	comboBox->setCurrentIndex(comboBox->findText(Value));
	blockSignals(false);
	emit changed(Value, false);
}

QString ComboBox::getValue()
{
	return comboBox->currentText();
}

void ComboBox::comboStateChanged(QString Value)
{
	emit changed(Value, true);
}



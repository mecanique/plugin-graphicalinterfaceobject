/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __SLIDER_H__
#define __SLIDER_H__

#include "GraphicalElement.h"
#include <QSlider>

/*
Classe de représentation d'un slider.
*/
class Slider : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	Slider(QString Text, QWidget *Parent=0);
	///Destructeur.
	~Slider();
	
	/**
	Fixe les propriétés d'un slider.
	*/
	void setProperties(bool Enabled, QString Text, int Min, int Max, int Step);
	/**
	Fixe la valeur d'un slider.
	*/
	void setValue(int Value);
	/**
	Retourne la valeur d'un slider.
	*/
	int getValue();
	
	signals:
	/**
	Est émis lorsque le slider change de valeur.
	*/
	void changed(int Value, bool User);
	
	private:
	///Slider.
	QSlider *slider;
	
	private slots:
	///Connecté à QSlider::valueChanged(int)
	void sliderStateChanged(int Value);
};

#endif /* __SLIDER_H__ */


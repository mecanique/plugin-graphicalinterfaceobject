/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "LineEdit.h"

LineEdit::LineEdit(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	lineEdit = new QLineEdit(this);
	setAttached(lineEdit);
	connect(lineEdit, SIGNAL(textEdited(QString)), SLOT(lineEditStateChanged(QString)));
}

LineEdit::~LineEdit()
{
}
	
void LineEdit::setProperties(bool Enabled, QString Text)
{
	lineEdit->setEnabled(Enabled);
	setText(Text);
}

void LineEdit::setValue(QString Value)
{
	lineEdit->setText(Value);
	emit changed(Value, false);
}

QString LineEdit::getValue()
{
	return lineEdit->text();
}

void LineEdit::lineEditStateChanged(QString Value)
{
	emit changed(Value, true);
}



/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __DIAL_H__
#define __DIAL_H__

#include "GraphicalElement.h"
#include <QDial>

/*
Classe de représentation d'un dial.
*/
class Dial : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	Dial(QString Text, QWidget *Parent=0);
	///Destructeur.
	~Dial();
	
	/**
	Fixe les propriétés du dial.
	*/
	void setProperties(bool Enabled, QString Text, int Min, int Max, int Step, bool Wrapping);
	/**
	Fixe la valeur du dial.
	*/
	void setValue(int Value);
	/**
	Retourne la valeur du dial.
	*/
	int getValue();
	
	signals:
	/**
	Est émis lorsque le dial change de valeur.
	*/
	void changed(int Value, bool User);
	
	private:
	///Dial.
	QDial *dial;
	
	private slots:
	///Connecté à QDial::valueChanged(int)
	void dialStateChanged(int Value);
};

#endif /* __DIAL_H__ */


/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __PROGRESSBAR_H__
#define __PROGRESSBAR_H__

#include "GraphicalElement.h"
#include <QProgressBar>

/*
Classe de représentation d'une progressbar.
*/
class ProgressBar : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	ProgressBar(QString Text, QWidget *Parent=0);
	///Destructeur.
	~ProgressBar();
	
	/**
	Fixe les propriétés de la progressbar.
	*/
	void setProperties(bool Enabled, QString Text, int Min, int Max, QString Format, bool Inverted);
	/**
	Fixe la valeur de la progressbar.
	*/
	void setValue(int Value);
	/**
	Retourne la valeur de la progressbar.
	*/
	int getValue();
	
	signals:
	/**
	Est émis lorsque la progressbar change de valeur.
	*/
	void changed(int Value);
	
	private:
	///ProgressBar.
	QProgressBar *progressBar;
};

#endif /* __PROGRESSBAR_H__ */


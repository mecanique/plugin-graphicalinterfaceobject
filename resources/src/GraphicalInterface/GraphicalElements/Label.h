/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __LABEL_H__
#define __LABEL_H__

#include "GraphicalElement.h"

/*
Classe de représentation d'un label.
*/
class Label : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	Label(QString Text, QWidget *Parent=0);
	///Destructeur.
	~Label();
	
	/**
	Fixe les propriétés du label.
	*/
	void setProperties(QString Text);

};

#endif /* __LABEL_H__ */


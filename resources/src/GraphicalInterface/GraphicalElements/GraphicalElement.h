/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __GRAPHICALELEMENT_H__
#define __GRAPHICALELEMENT_H__

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>

/**
\brief	Classe de représentation d'un élément graphique.
*/
class GraphicalElement : public QWidget
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	GraphicalElement(QString Text, QWidget *Parent=0);
	/**
	\brief	Destructeur.
	*/
	~GraphicalElement();
	
	protected:
	/**
	\brief	Assigne le widget représenté.
	*/
	void setAttached(QWidget *Widget);
	
	/**
	\brief	Modifie le texte affiché.
	*/
	void setText(QString Text);
	
	private:
	///Layout de mise en page.
	QHBoxLayout *layout;
	///Label d'affichage du texte.
	QLabel *label;
	
};

#endif /* __GRAPHICALELEMENT_H__ */


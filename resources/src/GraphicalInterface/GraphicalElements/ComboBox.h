/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __COMBOBOX_H__
#define __COMBOBOX_H__

#include "GraphicalElement.h"
#include <QComboBox>
#include "../../mecanique/string.h"

/*
Classe de représentation d'une combo box.
*/
class ComboBox : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	ComboBox(QString Text, QWidget *Parent=0);
	///Destructeur.
	~ComboBox();
	
	/**
	Fixe les propriétés de la combo box.
	*/
	void setProperties(bool Enabled, QString Text, QString Values);
	/**
	Fixe la valeur de la combo box.
	*/
	void setValue(QString Value);
	/**
	Retourne la valeur de la combo box.
	*/
	QString getValue();
	
	signals:
	/**
	Est émis lorsque la combo box change de valeur.
	*/
	void changed(string Value, bool User);
	
	private:
	///Combo box.
	QComboBox *comboBox;
	
	private slots:
	///Connecté à QComboBox::currentIndexChanged(QString)
	void comboStateChanged(QString Value);
};

#endif /* __COMBOBOX_H__ */


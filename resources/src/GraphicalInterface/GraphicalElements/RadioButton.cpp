/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "RadioButton.h"

RadioButton::RadioButton(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	widget = new QWidget(this);
	layout = new QGridLayout(widget);
	group = new QButtonGroup(this);
	setAttached(widget);
	connect(group, SIGNAL(buttonClicked(QAbstractButton*)), SLOT(buttonGroupStateChanged(QAbstractButton*)));
}

RadioButton::~RadioButton()
{
}
	
void RadioButton::setProperties(bool Enabled, QString Text, QString Values)
{
	widget->setEnabled(Enabled);
	setText(Text);
	
	while (group->buttons().size() != 0) {
		delete group->buttons().takeFirst();
	}
	
	QStringList tempItems = Values.split(';');
	for (int i=0 ; i < tempItems.size() ; i++) {
		QRadioButton *tempButton = new QRadioButton(tempItems.at(i), widget);
		
		if (i == 0) tempButton->setChecked(true);
		
		group->addButton(tempButton);
		
		layout->addWidget(tempButton, i / 2, i % 2);
	}
	
}

void RadioButton::setValue(QString Value)
{
	blockSignals(true);
	
	bool Find=false;
	for (int i=0 ; i < group->buttons().size() && !Find ; i++) {
		if (group->buttons().at(i)->text() == Value) {
			group->buttons().at(i)->clicked();
			Find = true;
		}
	}
	
	blockSignals(false);
	
	if (Find) emit changed(Value, false);
}

QString RadioButton::getValue()
{
	return (group->checkedButton() != 0) ? group->checkedButton()->text() : "";
}

void RadioButton::buttonGroupStateChanged(QAbstractButton *Button)
{
	emit changed(Button->text(), true);
}



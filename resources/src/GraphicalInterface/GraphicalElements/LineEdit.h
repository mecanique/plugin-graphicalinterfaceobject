/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __LINEEDIT_H__
#define __LINEEDIT_H__

#include "GraphicalElement.h"
#include <QLineEdit>
#include "../../mecanique/string.h"

/*
Classe de représentation d'un lineedit.
*/
class LineEdit : public GraphicalElement
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	*/
	LineEdit(QString Text, QWidget *Parent=0);
	///Destructeur.
	~LineEdit();
	
	/**
	Fixe les propriétés du lineedit.
	*/
	void setProperties(bool Enabled, QString Text);
	/**
	Fixe la valeur du lineedit.
	*/
	void setValue(QString Value);
	/**
	Retourne la valeur du lineedit.
	*/
	QString getValue();
	
	signals:
	/**
	Est émis lorsque le lineedit change de valeur.
	*/
	void changed(string Value, bool User);
	
	private:
	///LineEdit.
	QLineEdit *lineEdit;
	
	private slots:
	///Connecté à QLineEdit::textEdited(QString)
	void lineEditStateChanged(QString Value);
};

#endif /* __LINEEDIT_H__ */


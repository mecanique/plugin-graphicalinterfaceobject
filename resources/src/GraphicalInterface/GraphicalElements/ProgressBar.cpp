/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "ProgressBar.h"

ProgressBar::ProgressBar(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	progressBar = new QProgressBar(this);
	setAttached(progressBar);
}

ProgressBar::~ProgressBar()
{
}
	
void ProgressBar::setProperties(bool Enabled, QString Text, int Min, int Max, QString Format, bool Inverted)
{
	progressBar->setEnabled(Enabled);
	setText(Text);
	progressBar->setRange(Min, Max);
	progressBar->setFormat(Format);
	progressBar->setInvertedAppearance(Inverted);
}

void ProgressBar::setValue(int Value)
{
	progressBar->setValue(Value);
	emit changed(Value);
}

int ProgressBar::getValue()
{
	return progressBar->value();
}




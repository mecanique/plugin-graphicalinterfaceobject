/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "LCDNumber.h"

LCDNumber::LCDNumber(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	lcdNumber = new QLCDNumber(this);
	setAttached(lcdNumber);
}

LCDNumber::~LCDNumber()
{
}
	
void LCDNumber::setProperties(bool Enabled, QString Text, unsigned int Digits, unsigned int Base)
{
	lcdNumber->setEnabled(Enabled);
	setText(Text);
	lcdNumber->setDigitCount(Digits);
	switch (Base) {
		case 2:
		lcdNumber->setBinMode();
		break;
		case 8:
		lcdNumber->setOctMode();
		break;
		case 16:
		lcdNumber->setHexMode();
		break;
		case 10:
		default:
		lcdNumber->setDecMode();
		break;
	}
	
}

void LCDNumber::setValue(double Value)
{
	lcdNumber->display(Value);
	emit changed(Value);
}

double LCDNumber::getValue()
{
	return lcdNumber->value();
}




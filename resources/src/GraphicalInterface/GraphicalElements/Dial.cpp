/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Dial.h"

Dial::Dial(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	dial = new QDial(this);
	setAttached(dial);
	connect(dial, SIGNAL(valueChanged(int)), SLOT(dialStateChanged(int)));
}

Dial::~Dial()
{
}
	
void Dial::setProperties(bool Enabled, QString Text, int Min, int Max, int Step, bool Wrapping)
{
	dial->setEnabled(Enabled);
	setText(Text);
	dial->setRange(Min, Max);
	dial->setSingleStep(Step);
	dial->setWrapping(Wrapping);
}

void Dial::setValue(int Value)
{
	blockSignals(true);
	dial->setValue(Value);
	blockSignals(false);
	emit changed(Value, false);
}

int Dial::getValue()
{
	return dial->value();
}

void Dial::dialStateChanged(int Value)
{
	emit changed(Value, true);
}



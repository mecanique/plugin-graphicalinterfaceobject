/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "DoubleSpinBox.h"

DoubleSpinBox::DoubleSpinBox(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	doubleSpinBox = new QDoubleSpinBox(this);
	setAttached(doubleSpinBox);
	connect(doubleSpinBox, SIGNAL(valueChanged(double)), SLOT(doubleSpinBoxStateChanged(double)));
}

DoubleSpinBox::~DoubleSpinBox()
{
}
	
void DoubleSpinBox::setProperties(bool Enabled, QString Text, double Min, double Max, double Step, unsigned int Decimals, QString Prefix, QString Suffix)
{
	doubleSpinBox->setEnabled(Enabled);
	setText(Text);
	doubleSpinBox->setRange(Min, Max);
	doubleSpinBox->setSingleStep(Step);
	doubleSpinBox->setDecimals(Decimals);
	doubleSpinBox->setPrefix(Prefix);
	doubleSpinBox->setSuffix(Suffix);
}

void DoubleSpinBox::setValue(double Value)
{
	blockSignals(true);
	doubleSpinBox->setValue(Value);
	blockSignals(false);
	emit changed(Value, false);
}

double DoubleSpinBox::getValue()
{
	return doubleSpinBox->value();
}

void DoubleSpinBox::doubleSpinBoxStateChanged(double Value)
{
	emit changed(Value, true);
}



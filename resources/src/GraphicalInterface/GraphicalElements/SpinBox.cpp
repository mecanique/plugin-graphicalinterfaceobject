/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "SpinBox.h"

SpinBox::SpinBox(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	spinBox = new QSpinBox(this);
	setAttached(spinBox);
	connect(spinBox, SIGNAL(valueChanged(int)), SLOT(spinBoxStateChanged(int)));
}

SpinBox::~SpinBox()
{
}
	
void SpinBox::setProperties(bool Enabled, QString Text, int Min, int Max, int Step, QString Prefix, QString Suffix)
{
	spinBox->setEnabled(Enabled);
	setText(Text);
	spinBox->setRange(Min, Max);
	spinBox->setSingleStep(Step);
	spinBox->setPrefix(Prefix);
	spinBox->setSuffix(Suffix);
}

void SpinBox::setValue(int Value)
{
	blockSignals(true);
	spinBox->setValue(Value);
	blockSignals(false);
	emit changed(Value, false);
}

int SpinBox::getValue()
{
	return spinBox->value();
}

void SpinBox::spinBoxStateChanged(int Value)
{
	emit changed(Value, true);
}



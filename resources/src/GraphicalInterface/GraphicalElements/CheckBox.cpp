/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "CheckBox.h"

CheckBox::CheckBox(QString Text, QWidget *Parent) : GraphicalElement(Text, Parent)
{
	checkBox = new QCheckBox(this);
	setAttached(checkBox);
	connect(checkBox, SIGNAL(stateChanged(int)), SLOT(checkStateChanged(int)));
}

CheckBox::~CheckBox()
{
}
	
void CheckBox::setProperties(bool Enabled, QString Text)
{
	checkBox->setEnabled(Enabled);
	setText(Text);
}

void CheckBox::setValue(bool Checked)
{
	if (Checked != getValue()) {
		checkBox->setCheckState((Checked) ? Qt::Checked : Qt::Unchecked);
		emit changed(Checked, false);
		}
}

bool CheckBox::getValue()
{
	return (checkBox->checkState() == Qt::Checked) ? true : false;
}

void CheckBox::checkStateChanged(int State)
{
	emit changed(((State == Qt::Checked) ? true : false), true);
}



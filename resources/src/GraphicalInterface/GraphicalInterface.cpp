/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "GraphicalInterface.h"

GraphicalInterface::GraphicalInterface(QString Name, Project* const Project) : Object(Name, QString("GraphicalInterface"), Project)
{
	panel = new QWidget(Project, Qt::Dialog);
		panel->setWindowTitle(Name);
		panel->setAttribute(Qt::WA_DeleteOnClose, false);
	
	layout = new QVBoxLayout(panel);
		layout->setSpacing(0);
		layout->setSizeConstraint(QLayout::SetFixedSize);
	
	changeStatus(Object::Operational);
	changeInfos(tr("Interface loaded."));
}

GraphicalInterface::~GraphicalInterface()
{

}
	
void GraphicalInterface::more()
{
	panel->show();
}
	
QVariant GraphicalInterface::settings()
{
	return QVariant(panel->saveGeometry());
}

void GraphicalInterface::setSettings(QVariant Settings)
{
	panel->restoreGeometry(Settings.toByteArray());
}
	
void GraphicalInterface::addGraphicalElement(GraphicalElement *GE)
{
	layout->addWidget(GE);
}



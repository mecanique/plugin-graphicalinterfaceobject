/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __GRAPHICALINTERFACE_H__
#define __GRAPHICALINTERFACE_H__

#include <QtWidgets>
#include "../mecanique/Object.h"
#include "../mecanique/string.h"
#include "GraphicalElements/GraphicalElement.h"

class GraphicalInterface : public Object
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Name	Nom de l'objet.
	\param	Projet	Projet d'appartenance.
	*/
	GraphicalInterface(QString Name, Project* const Project);
	/**
	\brief	Destructeur.
	*/
	~GraphicalInterface();
	
	///Ouvre l'interface graphique.
	void more();
	
	///Retourne les paramètres de vue.
	QVariant settings();
	///Charge les paramètres de vue.
	void setSettings(QVariant Settings);
	
	protected:
	///Ajoute un élément graphique à l'interface.
	void addGraphicalElement(GraphicalElement *GE);
	
	private:
	///Panneau d'affichage.
	QWidget *panel;
	///Layout du panneau d'affichage.
	QVBoxLayout *layout;
};

#endif /* __GRAPHICALINTERFACE_H__ */


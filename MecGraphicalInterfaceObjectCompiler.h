/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECGRAPHICALINTERFACEOBJECTCOMPILER_H__
#define __MECGRAPHICALINTERFACEOBJECTCOMPILER_H__

#include <MecObjectCompiler.h>
#include <QMap>

/**
\brief	Classe de compilation d'un objet GraphicalInterface.
*/
class MecGraphicalInterfaceObjectCompiler : public MecObjectCompiler
{
public:
	/**
	\brief	Constructeur.
	\param	Object	Objet compilé, doit absolument exister lors de la construction (c.-à-d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecGraphicalInterfaceObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler);
	///Destructeur.
	~MecGraphicalInterfaceObjectCompiler();

	///Retourne la liste des noms et types d'éléments graphiques dans l'objet compilé.
	QMap<QString, QString> containedElements() const;

	///Retourne la liste des noms de type d'élément graphique.
	static QStringList typesAvailable();

	/**
	Retourne la liste des ressources à ajouter au répertoire de compilation pour compiler cet élément.
	*/
	virtual QList<QResource*> resources();
	/**
	Retourne les instructions à ajouter au fichier projet (".pro").
	*/
	virtual QString projectInstructions();

	/**
	Retourne le contenu du header de l'élément.
	*/
	virtual QString header();
	/**
	Retourne le contenu du fichier d'implémentation de l'élément.
	*/
	virtual QString source();

private:
	QMap<QString, QString> m_containedElements;
};

#endif /* __MECGRAPHICALINTERFACEOBJECTCOMPILER_H__ */


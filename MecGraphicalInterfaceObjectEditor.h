/*
© Quentin VIGNAUD, 2014

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECGRAPHICALINTERFACEOBJECTEDITOR_H__
#define __MECGRAPHICALINTERFACEOBJECTEDITOR_H__

#include <MecObjectEditor.h>
#include "MecGraphicalInterfaceFunctionEditor.h"
#include "MecGraphicalInterfaceSignalEditor.h"

/**
\brief	Classe d'édition d'objet de type « GraphicalInterface ».
*/
class MecGraphicalInterfaceObjectEditor : public MecObjectEditor
{
	Q_OBJECT

public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecGraphicalInterfaceObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecGraphicalInterfaceObjectEditor();

	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;

	///Retourne la liste des noms d'éléments graphiques actuellement dans l'éditeur.
	QStringList actualElementsNames() const;

	///Retourne la liste des noms de type d'élément graphique.
	static QStringList typesAvailable();

public slots:
	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);

	///Appelle chooseGraphicalElement().
	void addFunction();
	///Appelle chooseGraphicalElement().
	void addSignal();

	///Ne fait rien.
	void addVariable();

signals:


private:
	/**
	\brief	Demande à l'utilisateur quel élément graphique ajouter.

	Ajoute l'élément graphique correspondant si validé par l'utilisateur.
	*/
	void chooseGraphicalElement();

private slots:
	/**
	\brief	Supprime les éléments groupés avec l'élément spécifié.

	Est connecté au signal destroyed(MecAbstractElement*) des éléments enfants de l'objet GraphicalInterface édité.
	Le but de cette procédure est de conserver la cohérence des éléments graphiques existants dans l'objet.
	*/
	void removeGraphicalElement(MecAbstractElement *Element);


};

/**
\brief	Classe de choix de nouvel élément graphique.
*/
class MecGraphicalInterfaceObjectEditorChooser : public QDialog
{
	Q_OBJECT

public:
	///Constructeur.
	MecGraphicalInterfaceObjectEditorChooser(QWidget * Parent=0, Qt::WindowFlags F=0);
	///Destructeur.
	~MecGraphicalInterfaceObjectEditorChooser();

	///Retourne, s'il y a lieu, le choix de l'utilisateur.
	QString choice() const;
	///Retourne le nom entré par l'utilisateur.
	QString nameChoosed() const;

private:
	///PushButton.
	QPushButton *pushButton;
	///Validateur PushButton
	QPushButton *buttonPushButton;

	///CheckBox.
	QCheckBox *checkBox;
	///Validateur CheckBox.
	QPushButton *buttonCheckBox;

	///RadioButton.
	QRadioButton *radioButton;
	///Validateur RadioButton.
	QPushButton *buttonRadioButton;

	///ComboBox.
	QComboBox *comboBox;
	///Validateur ComboBox.
	QPushButton *buttonComboBox;

	///SpinBox.
	QSpinBox *spinBox;
	///Validateur SpinBox.
	QPushButton *buttonSpinBox;

	///DoubleSpinBox.
	QDoubleSpinBox *doubleSpinBox;
	///Validateur DoubleSpinBox.
	QPushButton *buttonDoubleSpinBox;

	///Slider.
	QSlider *slider;
	///Validateur Slider.
	QPushButton *buttonSlider;

	///Dial.
	QDial *dial;
	///Validateur Dial.
	QPushButton *buttonDial;

	///LineEdit.
	QLineEdit *lineEdit;
	///Validateur LineEdit.
	QPushButton *buttonLineEdit;

	///Label.
	QLabel *label;
	///Validateur Label.
	QPushButton *buttonLabel;

	///ProgressBar.
	QProgressBar *progressBar;
	///Validateur ProgressBar.
	QPushButton *buttonProgressBar;

	///LCDNumber.
	QLCDNumber *LCDNumber;
	///Validateur LCDNumber.
	QPushButton *buttonLCDNumber;


	///Label de nom.
	QLabel *labelName;
	///LineEdit de nom.
	QLineEdit *lineEditName;
	///Validateur de nom.
	QRegExpValidator *regExpValidatorName;
	///Bouton d'annulation.
	QPushButton *buttonCancel;

	///Séparateur du haut.
	QFrame *frameTopLine;
	///Séparateur du bas.
	QFrame *frameBottomLine;

	///Layout général.
	QGridLayout *layoutMain;

	///Choix de l'utilisateur.
	QString m_choice;
	///Nom entré par l'utilisateur.
	QString m_nameChoosed;

private slots:
	/**
	\brief	Assigne m_choice à la valeur du texte du bouton appelant et m_nameChoosed au contenu de lineEditName.

	Est connecté au signal clicked() des boutons de choix.

	Appelle ensuite la méthode accept() héritée de QDialog.
	*/
	void choose();
	/**
	\brief	Vérifie que lineEditName ne soit pas vide.

	Est connecté au signal textEdited() de lineEditName.
	*/
	void checkEditName(QString Text);
};

#endif /* __MECGRAPHICALINTERFACEOBJECTEDITOR_H__ */


######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = graphicalinterfaceobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/graphicalinterfaceobject.fr.ts

OTHER_FILES += metadata.json

HEADERS += \
	MecGraphicalInterfaceObjectPlugin.h \
	MecGraphicalInterfaceObjectCompiler.h \
	MecGraphicalInterfaceObjectEditor.h \
	MecGraphicalInterfaceFunctionEditor.h \
	MecGraphicalInterfaceSignalEditor.h 

SOURCES += \
	MecGraphicalInterfaceObjectPlugin.cpp \
	MecGraphicalInterfaceObjectCompiler.cpp \
	MecGraphicalInterfaceObjectEditor.cpp \
	MecGraphicalInterfaceFunctionEditor.cpp \
	MecGraphicalInterfaceSignalEditor.cpp 

